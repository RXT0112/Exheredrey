#!/bin/sh
# shellcheck shell=sh # Written to comply with POXIS IEEE 1003.1-2017

# Created by Jacob Hrbek identified by GPG identifier assigned to the electronic mail <kreyren@rixotstudio.cz> according to the keyserver <https://keys.openpgp.org> in 19/10/2020-EU 22:38:02 CEST

# FIXME(Krey): This has to have extension '.bash' for paludis to interpret it, change paludis to recognize '.sh'

###! This is a configuration file that returns 'conf' format in stdout for paludis interpretation
###! stderr will be outputted in the terminal and not processed

###! Paludis INFO(pinfo) is a function used in options.bash to provide output for the end-user
pinfo() { ${PRINTF:-printf} '%s\n' "$1" >&2 ;}

###! This auto-configuration takes few mili-seconds from each paludis start and as so it can be skipped using variable 'PALUDIS_SKIP_AUTOCONF' set to value 'true' for respected architecture
case "$PALUDIS_SKIP_AUTOCONF" in
	# NOTE(Krey): This expression looks ugly, but it returns integer for false depending on what the architecture consideres as false
	"$(${FALSE:-false}; ${PRINTF:-printf} "$?")")
		# FIXME-TRANSLATE
		case "$LANG" in
			en-*|*)
				pinfo "Performing auto-configuration of paludis for used system, this can be disabled using 'PALUDIS_SKIP_AUTOCONF' environment variable"
		esac

		# Set available CPU threads for the compilation
		if [ -z "$PALUDIS_SYSTEM_JOBS" ]; then
			case "${KERNEL:-$(uname -s)}" in
				linux)
					if ! nproc; then
						pinfo "Using command '${NPROC:-nproc}' which returns value '$(${NPROC:-nproc})' for CPU thread count optimization"
						${PRINTF:-printf} "*/* BUILD_OPTIONS: jobs=%s\\n" "${NPROC:-nproc}"
					else
						pinfo "Command '${NPROC:-nproc}' is not available on this system, unable to deduce CPu thread count for optimization and failing to '1' which may impact efficiency on systems with more then 1 threads"
						${PRINTF:-printf} "*/* BUILD_OPTIONS: jobs=%s\\n" "$(${PRINTF:-printf} 1)"
					fi
				;;
				*) pinfo "Kernel '${KERNEL:-$(uname -s)}' is not implemented in logic to provide CPU threads"
			esac
		elif [ -z "$PALUDIS_SYSTEM_JOBS" ]; then
			${PRINTF:-printf} "*/* BUILD_OPTIONS: jobs=%s\\n" "$PALUDIS_SYSTEM_JOBS"
		else
			die integrity "Command 'test' doesn't return a sane value on this system to provide CPU thread count"
		fi

		if [ -z "$PALUDIS_SYSTEM_JOBS" ]; then
			# NOTE(Krey): Try to get the value from 'nproc' which by default returns integer of available processors
			if ! nproc; then
				${PRINTF:-printf} "*/* BUILD_OPTIONS: jobs=%s\\n" "${NPROC:-nproc}"
			else
				${PRINTF:-printf} "*/* BUILD_OPTIONS: jobs=%s\\n" "$(${PRINTF:-printf} 1)"
			fi
		elif [ -n "$PALUDIS_SYSTEM_JOBS" ]; then
			${PRINTF:-printf} "*/* BUILD_OPTIONS: jobs=%s\\n" "$PALUDIS_SYSTEM_JOBS"
		else
			die integrity "Command 'test' doesn't return sane value on this system to identify PALUDIS_SYSTEM_JOBS variable"
			exit 1 # In case 'die' is not available
		fi

		# Identify CPU flags
		case "${KERNEL:-$(uname -s)}" in
				linux)
					###! On linux we are trying to read /proc/cpuinfo assuming it being enabled in the kernel configuration
					if [ -f /proc/cpuinfo ]; then
						foundFlags="$(${CAT:-cat} /proc/cpuinfo  | ${GREP:-grep} -m 1 flags | ${SED:-sed} 's/^flags.*: //g')"
						
						case "${ARCH:-$(uname -m)}" in
							x86_64)
								${PRINTF:-printf} "*/* AMD64_CPU_FEATURES: %s\\n" "$foundFlags"
								${PRINTF:-printf} "*/* X86_CPU_FEATURES: %s\\n" "$foundFlags"
							;;
							x86) ${PRINTF:-printf} "*/* X86_CPU_FEATURES: %s\\n" "$foundFlags" ;;
							*) pinfo "This architecture '${ARCH:-$(uname -m)}' is not implemented, unable to determine CPU flags to be used in compilation"
						esac
					elif [ ! -f /proc/cpuinfo ]; then
						pinfo "File '/proc/cpuinfo' is not present on this linux system, unable to determine CPU flags to be used in compilation"
					else
						die integrity "Command 'test' doesn't return sane value on this system to process CPU flags"
						exit 1 # In case 'die' is not available
					fi
				;;
				*) pinfo "Kernel '${KERNEL:-$(uname -s)}' is not implemented for logic to provide CPU flags"
			esac
	;;
	"$(${TRUE:-true}; ${PRINTF:-printf} "$?")"|*) pinfo "Variable 'PALUDIS_SKIP_AUTOCONF' is set to value to skip autoconfiguration, skipping.."
esac