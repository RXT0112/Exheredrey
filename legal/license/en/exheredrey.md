Copyright (C) 2017-2020 - All rights reserved by Jacob Hrbek identified using a GPG identifier assigned to the electronic mail <kreyren@rixotstudio.cz> according to the keyserver <https://keys.openpgp.org> unless stated otherwise at the header of the file

END-OF-LICENSE 

---
This is a stub implementation for four freedom respecting license and the content below `END-OF-LICENSE` is currently considered as non-legally binding
---