# RiXotStudio's Zernit - Exheredrey project

Libre Open-Source (LOSS) fork of Exherbo GNU/Linux <https://exherbo.org> focused on resolving code quality issues while arguably upgrading the downstream codebase to comply with POSIX IEEE 1003.1-2017 standard and maintaining a productive work environment with the intention to educate each other while providing a peer-review of the code.

## Economy

This fork is designed to be non-free while complying with Free Software Foundations's essential four freedoms to allow for the required maintainance and expected quality of the distribution on which we woudn't have the resources if it was provided for free.

Currently this fork is free to use which will change in the future depending on popularity.

## Future of this project

This is research implementation of the Zernit project <https://github.com/RXT0112> to provide the required infrastracture to develop the project and stress test the implementation and ideology and as such this project has set end-of-life at which it will be most likely released as Free Libre Open-Source software with our effort focused on RXT0112-PackageManager.